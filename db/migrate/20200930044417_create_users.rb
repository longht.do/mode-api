class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.boolean :is_creator
      t.string :username
      t.string :insta_username
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :discount_code

      t.timestamps
    end
  end
end
