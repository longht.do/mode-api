class UsersController < ApplicationController
  def signup
    # 1. create a new user and save to the database
    new_user_params = user_params
    if (new_user_params["is_creator"])
      new_user_params["discount_code"] = Discount.discount_code
    end

    @user = User.create!(new_user_params)

    # 2. invoke the helper method to send a request to Klaviyo
    Klaviyo.add_member_to_list(@user)
    # 3. return a response
    render json: {data: @user}, status: :created
  end

  private

  def user_params
    #whitelists params
    params.permit(:is_creator, :username, :insta_username, :first_name, :last_name, :email, :discount_code)
  end
end
