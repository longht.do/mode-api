class User < ApplicationRecord
  validates_presence_of :username, :first_name, :last_name, :email
end
