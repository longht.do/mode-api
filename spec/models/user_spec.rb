require 'rails_helper'

# Test suite for the User model
RSpec.describe User, type: :model do
  # Validation test
  it { should validate_presence_of(:email) }

  # pending "add some examples to (or delete) #{__FILE__}"
end
