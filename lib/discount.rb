class Discount
  def self.discount_code()
    charset = Array('A'..'Z') + Array('a'..'z')
    Array.new(7) { charset.sample }.join
  end
end