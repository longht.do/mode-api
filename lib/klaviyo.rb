class Klaviyo
  def self.add_member_to_list(user)
    options = {
      headers: {"Content-Type" => "application/json"},
      body: {
        api_key: "pk_3ee38c812926091b7c3cb517b6ddff7c9d",
        profiles: [
          {
            email: user.email,
            first_name: user.first_name,
            last_name: user.last_name,
            username: user.username,
            is_creator: user.is_creator,
            insta_username: user.insta_username
          }
        ]
      }.to_json
    }

    creator_list_id = 'TcFgtf'
    user_list_id = 'XxMAD7'
    list_id = user.is_creator ? creator_list_id : user_list_id
    url = "https://a.klaviyo.com/api/v2/list/#{list_id}/members"
    HTTParty.post(url, options)
  end
end

